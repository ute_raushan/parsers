#!/usr/bin/env python
# coding: utf-8

# In[1]:


# import libraries
import psycopg2
import pandas as pd
import requests
from bs4 import BeautifulSoup
import warnings
from tqdm import tqdm 
from time import sleep
import os
import json
from datetime import date
from datetime import datetime
from kzt_exchangerates import Rates
from io import StringIO

warnings.filterwarnings("ignore", message="The default value of regex will change from True to False in a future version.")


# In[8]:


# a function to connect to PostgreSQL
def create_connection(db_name, db_user, db_password, db_host, db_port):
    connection = None
    try:
        connection = psycopg2.connect(
            database=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
        )
        print("Connection to PostgreSQL DB successful")
    except OperationalError as e:
        print(f"The error '{e}' occurred")
    return connection


# connect to DB
connection = create_connection("rwh_datalake", "rwh_analytics", "4HPzQt2HyU@", "172.30.227.205", "5439")


# In[3]:


# a function to read a table into a pandas DataFrame
def read_table_to_df(connection, table_name):
    query = f"SELECT * FROM {table_name}"
    df_from_DB = pd.read_sql(query, connection)
    return df_from_DB


# read tables from DB
table_name = "analytics_hh_areas"
df_areas = read_table_to_df(connection, table_name)

table_name = "analytics_hh_roles"
df_roles = read_table_to_df(connection, table_name)


# In[4]:


# get today's date (data parsed date)
today = date.today()

# Format today's date as 'yyyy-mm-dd'
parsed_date = today.strftime('%Y-%m-%d') # Output: e.g., '2023-08-20'

# Format today's date as 'dd.mm.yyyy'
parsed_date2 = today.strftime('%d.%m.%Y') # Output: e.g., '20.08.2023'


# get exchange rate from RUBLE to KZ TENGE
rates = Rates()
exchange_rate = rates.get_exchange_rate("RUB", from_kzt=True, date=parsed_date2)

# create an empty df for all cities
df_all = pd.DataFrame()


# In[5]:


# create an empty df
df = pd.DataFrame()

# iterate for each area
for j in range(len(df_areas)):  
    city_id = df_areas['hh_id'][j]  
    city_name = df_areas['city_ru'][j]
    print(city_name)
    
    for i in range(len(df_roles)): 
        # hh api throws 'connection error' after over time
        # if the error appears, parser continues after a while
        while True:
            try:                
                role_id = df_roles['id'][i]
                role = df_roles['role_ru'][i]  
                base_url = "https://hh.ru/search/vacancy?"
                params = {
                    "text": role,
                    "salary": "",
                    "area": city_id,
                    "ored_clusters": "true"
                }
                query_string = "&".join([f"{key}={value}" for key, value in params.items()])
                url = base_url + query_string
                headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:76.0) Gecko/20100101 Firefox/76.0'}

                # ceate a session
                session = requests.Session()
                r = session.get(url, timeout=30, headers=headers)
                soup = BeautifulSoup(r.content, 'html.parser')
                dates_1 = soup.find_all("li", class_="novafilters-list__item")
                dates_2 = soup.find_all("span", class_="bloko-text bloko-text_tertiary")
                data = []
                for date_1, date_2 in zip(dates_1, dates_2):
                    date_1_text = date_1.text.strip()
                    data.append((date_1_text))

                df_temp = pd.DataFrame(data, columns=['Fil'])
                df_temp["Value"] = df_temp["Fil"].str.extract(r'(\d+\s*\d*)$')
                df_temp["Value"] = df_temp["Value"].str.replace(' ', '').astype(str)
                df_temp['Fil'] = df_temp['Fil'].str.replace(r'\d+\s*\d*$', '').str.strip()
                df_temp['role_id'] = [role_id] * len(df_temp)
                df = pd.concat([df, df_temp], axis=0)
                break
                
            except:
                # pause between requests to avoid 'connection error'
                sleep(60)
                pass
                
    df = df.pivot_table(index='role_id', columns='Fil',values = "Value", aggfunc=lambda x: x.max())

    # add categories to df (https://api.hh.ru/professional_roles)
    # df = df_roles.join(df.set_index('role_ru'), on='role_ru')
    
    df = df.merge(df_roles, left_on='role_id', right_on='id', how='inner')

    # rename columns (containing salaries) to int
    int_cols = []
    count = 0
    for col in df.columns:
        if count == 0:
            if city_name in col:
                df = df.rename(columns={col: 'vacancy_num'})
                count = 1

        if 'руб.' in col:
            col_salary = int(col[2:-4].replace(" ", ""))
            df = df.rename(columns={col: col_salary})
            int_cols.append(col_salary)

        elif '₽' in col:
            col_salary = int(col[2:-1].replace(" ", ""))
            df = df.rename(columns={col: col_salary})
            int_cols.append(col_salary)


    # sort columns containing salaries
    int_cols.sort(reverse=True)
    sorted_df = df[int_cols]

    sorted_df['salary'] = 0
    sorted_df['vacancy_num_with_salary'] = 0

    # calculate median salary and number of vacancies with salary
    if len(int_cols) != 0:
        for col in sorted_df.columns:
            if type(col) == int:
                if sorted_df[col].dtype == 'O':
                    sorted_df[col].fillna('0', inplace=True)
                    sorted_df.loc[:, col] = sorted_df[col].str.replace('\u202f', '')

                else:
                    sorted_df[col].fillna(0, inplace=True)

                sorted_df.loc[:, col] = sorted_df[col].astype(int)

                sorted_df.loc[sorted_df[col]!=0, 'salary'] += (sorted_df[col]-sorted_df['vacancy_num_with_salary']) * col
                sorted_df.loc[sorted_df[col]!=0, 'vacancy_num_with_salary'] = sorted_df[col]

        sorted_df.loc[sorted_df['vacancy_num_with_salary'] != 0, 'salary'] /= sorted_df['vacancy_num_with_salary']


        # change 'salary' from RUBLE to KZ TENGE
        sorted_df['salary'] = sorted_df['salary'] * exchange_rate
        sorted_df['salary'] = round(sorted_df['salary'] / 100) * 100
        sorted_df.loc[:, 'salary'] = sorted_df['salary'].astype(int)

    # rename column
    df = df.rename(columns={'id': 'role_id'})

    # list of columns required for analysis
    required_cols = ['role_id', 'vacancy_num', 'parsed_date', 'city_id',
                     'Временная работа', 'Неполный день', 'От 4 часов в день', 'По вечерам', 'Разовое задание', 'По выходным',
                     'Не\xa0требуется или не\xa0указано', 'Высшее', 'Среднее профессиональное',
                     'От\xa01\xa0года до\xa03\xa0лет', 'От\xa03\xa0до\xa06\xa0лет', 'Нет опыта', 'Более 6\xa0лет',
                     'Полная занятость', 'Частичная занятость', 'Стажировка', 'Проектная работа', 'Волонтерство',
                     'Полный день', 'Удаленная работа', 'Гибкий график', 'Сменный график', 'Вахтовый метод',
                     'salary', 'vacancy_num_with_salary'
                    ]

    # keep only required columns
    df = df.reindex(columns=required_cols)

    # assign values to columns
    df['city_id'] = city_id
    df['parsed_date'] = parsed_date
    df['salary'] = sorted_df['salary']
    df['vacancy_num_with_salary'] = sorted_df['vacancy_num_with_salary']

    # convert columns (except 'prof_role') to int
    for col in df.columns:
        if col not in ['role_id', 'city_id', 'parsed_date']:                            
            if df[col].dtype == 'object':
                df[col] = df[col].replace('nan', '0').fillna('0')
                df.loc[:, col] = df[col].str.replace('\u202f', '')

            else:
                df[col].fillna(0, inplace=True)

            df.loc[:, col] = df[col].astype(int)

    # drop rows where 'vacancy_num' is 0
    df = df.drop(df[df['vacancy_num'] == 0].index)

    # append to the main df
    df_all = pd.concat([df_all, df])


# In[6]:


# drop rows where vacancy_num is Nan
df_all = df_all.dropna(subset=['vacancy_num'])


# In[13]:


# a function to connect to PostgreSQL AGAIN because connection might get lost
def create_connection(db_name, db_user, db_password, db_host, db_port):
    connection = None
    try:
        connection = psycopg2.connect(
            database=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
        )
        print("Connection to PostgreSQL DB successful")
    except OperationalError as e:
        print(f"The error '{e}' occurred")
    return connection


# connect to DB
connection = create_connection("rwh_datalake", "rwh_analytics", "4HPzQt2HyU@", "172.30.227.205", "5439")


# In[14]:


# Append data from DataFrame to the existing table
def append_data_to_postgres(df, tablename, conn):
    """
    Append a DataFrame to a PostgreSQL table using psycopg2.

    Parameters:
    - df: Pandas DataFrame
    - tablename: PostgreSQL table name
    - connection_string: PostgreSQL connection string
    """
    # Establish connection
    cursor = conn.cursor()

    # Use StringIO to convert dataframe into a file object so we can use copy_from
    output = StringIO()
    df.to_csv(output, sep='\t', header=False, index=False)
    output.seek(0)

    # Insert data into table
    cursor.copy_from(output, tablename, columns=tuple(df.columns), null="")  # null values become ''
    conn.commit()

    # Close connection
    cursor.close()
    conn.close()

append_data_to_postgres(df_all, 'analytics_hh_all_cities', connection)


# In[ ]:





# In[ ]:





# In[ ]:




